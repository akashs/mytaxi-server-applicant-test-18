package com.mytaxi.service.test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.ManufacturerRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.impl.DefaultCarService;
import com.mytaxi.test.TestData;

public class CarServiceTest extends TestData {

	@Mock
	private CarRepository carRepository;

	@Mock
	private ManufacturerRepository manufacturerRepository;

	@InjectMocks
	private DefaultCarService carService;

	@BeforeClass
	public static void setUp() {
		MockitoAnnotations.initMocks(DefaultCarService.class);
	}

	@Test
	public void testFindCarById() throws EntityNotFoundException {
		CarDO car = getCar();
		when(carRepository.findById(any(Long.class))).thenReturn(Optional.of(car));
		carService.findCarById(any(Long.class));
		verify(carRepository, times(1)).findById(any(Long.class));
	}

	@Test
	public void testFindAllCars() {
		Iterable<CarDO> cars = Collections.singletonList(getCar());
		when(carRepository.findAll()).thenReturn(cars);
		carService.findAllCars();
		verify(carRepository, times(1)).findAll();
	}

	@Test
	public void testUpdate() throws Exception {
		CarDO car = getCar();
		ManufacturerDO manufacturer = getManufacturer();
		when(carRepository.findById(any(Long.class))).thenReturn(Optional.ofNullable(car));
		when(manufacturerRepository.findByName(any(String.class))).thenReturn(manufacturer);
		carService.updateCar(car);
		verify(carRepository, times(1)).findById(any(Long.class));
		verify(manufacturerRepository, times(1)).findByName(any(String.class));
	}

	@Test
	public void testDelete() throws EntityNotFoundException {
		CarDO car = getCar();
		when(carRepository.findById(any(Long.class))).thenReturn(Optional.ofNullable(car));
		carService.deleteCar(1L);
		verify(carRepository, times(1)).findById(any(Long.class));
	}
}
