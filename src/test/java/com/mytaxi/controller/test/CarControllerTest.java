package com.mytaxi.controller.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytaxi.controller.CarController;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.service.driver.CarService;
import com.mytaxi.test.TestData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

public class CarControllerTest extends TestData {

	private static final ObjectMapper mapper = new ObjectMapper();

	private MockMvc mvc;

	@Mock
	private CarService carService;

	@InjectMocks
	private CarController carController;

	@BeforeClass
	public static void setUp() {
		MockitoAnnotations.initMocks(CarController.class);
	}

	@Before
	public void init() {
		mvc = MockMvcBuilders.standaloneSetup(carController).dispatchOptions(true).build();
	}

	@Test
	public void testGetCar() throws Exception {
		CarDO CarDO = TestData.getCar();
		doReturn(CarDO).when(carService).findCarById(any(Long.class));
		carController.findCarById(10L);
		MvcResult result = mvc.perform(get("/v1/cars/{carId}", 10)).andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();
		final String responseBody = result.getResponse().getContentAsString();
		Assert.assertTrue(responseBody.contains("KL-11 4774"));
	}

	@Test
	public void getAllCars() throws Exception {
		List<CarDO> cars = Collections.singletonList(TestData.getCar());
		doReturn(cars).when(carService).findAllCars();
		carController.findAllCars();
		MvcResult result = mvc.perform(get("/v1/cars")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		final String responseBody = result.getResponse().getContentAsString();
		Assert.assertTrue(responseBody.contains("KL-11 4774"));
	}

	@Test
	public void createCar() throws Exception {
		CarDO carDO = TestData.getCar();
		CarDTO carDTO = TestData.getCarDTO();
		doReturn(carDO).when(carService).createCar(any(CarDO.class));
		carController.createCar(carDTO);
		String jsonInString = mapper.writeValueAsString(carDTO);
		MvcResult result = mvc.perform(post("/v1/cars").contentType(MediaType.APPLICATION_JSON).content(jsonInString))
				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();
		final String responseBody = result.getResponse().getContentAsString();
		Assert.assertTrue(responseBody.contains("Audi"));
	}

	@Test
	public void updateCar() throws Exception {
		CarDTO CarDTO = TestData.getCarDTO();
		doNothing().when(carService).updateCar(any(CarDO.class));
		carController.updateCar(CarDTO.getId(), CarDTO);
		String jsonInString = mapper.writeValueAsString(CarDTO);
		MvcResult result = mvc.perform(
				put("/v1/cars/{carId}", CarDTO.getId()).contentType(MediaType.APPLICATION_JSON).content(jsonInString))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
	}

	@Test
	public void deleteCar() throws Exception {
		doNothing().when(carService).deleteCar(any(Long.class));
		carController.deleteCar(1L);
		MvcResult result = mvc.perform(delete("/v1/cars/{carId}", 1L)).andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();
		Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
	}

}
