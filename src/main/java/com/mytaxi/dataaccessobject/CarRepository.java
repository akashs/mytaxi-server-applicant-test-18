package com.mytaxi.dataaccessobject;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.mytaxi.domainobject.CarDO;

public interface CarRepository extends CrudRepository<CarDO, Long> {

	Optional<CarDO> findByLicensePlate(String licensePlate);

	List<CarDO> findByrating(Float rating);
}
