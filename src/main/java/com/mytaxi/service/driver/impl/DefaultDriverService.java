package com.mytaxi.service.driver.impl;

import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.CarService;
import com.mytaxi.service.driver.DriverService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to encapsulate the link between DAO and controller and to have
 * business logic for some driver specific things.
 * <p/>
 */
@Service
public class DefaultDriverService implements DriverService {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

	private CarService carService;

	private final DriverRepository driverRepository;

	@Autowired
	public DefaultDriverService(final DriverRepository driverRepository, final CarService carService) {
		this.driverRepository = driverRepository;
		this.carService = carService;
	}

	/**
	 * Selects a driver by id.
	 *
	 * @param driverId
	 * @return found driver
	 * @throws EntityNotFoundException
	 *             if no driver with the given id was found.
	 */
	@Override
	public DriverDO findDriverById(Long driverId) throws EntityNotFoundException {
		return findDriverChecked(driverId);
	}

	/**
	 * Creates a new driver.
	 *
	 * @param driverDO
	 * @return
	 * @throws ConstraintsViolationException
	 *             if a driver already exists with the given username, ... .
	 */
	@Override
	public DriverDO createDriver(DriverDO driverDO) throws ConstraintsViolationException {
		DriverDO driver;
		try {
			driver = driverRepository.save(driverDO);
		} catch (DataIntegrityViolationException e) {
			LOG.warn("ConstraintsViolationException while creating a driver: {}", driverDO, e);
			throw new ConstraintsViolationException(e.getMessage());
		}
		return driver;
	}

	/**
	 * Deletes an existing driver by id.
	 *
	 * @param driverId
	 * @throws EntityNotFoundException
	 *             if no driver with the given id was found.
	 */
	@Override
	@Transactional
	public void deleteDriver(Long driverId) throws EntityNotFoundException {
		DriverDO driverDO = findDriverChecked(driverId);
		driverDO.setDeleted(true);
	}

	/**
	 * Update the location for a driver.
	 *
	 * @param driverId
	 * @param longitude
	 * @param latitude
	 * @throws EntityNotFoundException
	 */
	@Override
	@Transactional
	public void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException {
		DriverDO driverDO = findDriverChecked(driverId);
		driverDO.setCoordinate(new GeoCoordinate(latitude, longitude));
	}

	/**
	 * Find all drivers by online state.
	 *
	 * @param onlineStatus
	 */
	@Override
	public List<DriverDO> findDriverByOnlineStatus(OnlineStatus onlineStatus) {
		return driverRepository.findByOnlineStatus(onlineStatus);
	}

	private DriverDO findDriverChecked(Long driverId) throws EntityNotFoundException {
		return driverRepository.findById(driverId)
				.orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + driverId));
	}

	@Override
	@Transactional
	public void updateDriverCar(long driverId, String licensePlate)
			throws CarAlreadyInUseException, EntityNotFoundException {
		DriverDO driverDO = findDriverChecked(driverId);
		if (licensePlate == null) {
			CarDO carDO = carService.findCarByLicensePlate(driverDO.getCar().getLicensePlate());
			driverDO.setCar(null);
			carDO.setDriver(null);
			carService.updateCar(carDO);
		} else {
			CarDO carDO = carService.findCarByLicensePlate(licensePlate);
			driverDO.setCar(carDO);
			if (carDO.getDriver() == null) {
				carDO.setDriver(driverDO);
				carService.updateCar(carDO);
			}
			if (carDO.getDriver().getId().equals(driverId))
				return;
			throw new CarAlreadyInUseException("Car : '" + carDO.getLicensePlate() + "' is Already in use");
		}
	}

	@Override
	public List<DriverDO> findDriverByNameLicenseRating(String username, String licensePlate, Float rating) {
		if (username != null) {
			return driverRepository.findByUsername(username);
		} else if (licensePlate != null) {
			try {
				return Arrays.asList(carService.findCarByLicensePlate(licensePlate).getDriver());
			} catch (EntityNotFoundException e) {
				return new ArrayList<>();
			}
		} else if (rating != null) {
			List<DriverDO> driverList = new ArrayList<>();
			List<CarDO> carList = carService.findByRating(rating);
			for (CarDO car : carList) {
				if (car.getDriver() != null)
					driverList.add(car.getDriver());
			}
			LOG.info("findByParams output: {}", driverList);
			return driverList;
		}
		return null;
	}
}
