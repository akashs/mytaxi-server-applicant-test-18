package com.mytaxi.service.driver.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.ManufacturerRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.CarService;

@Service
public class DefaultCarService implements CarService {

	@Autowired
	private CarRepository carRepository;

	@Autowired
	private ManufacturerRepository manufacturerRepository;

	@Override
	public CarDO findCarById(final Long carId) throws EntityNotFoundException {
		return carCheck(carId);
	}

	@Override
	public Iterable<CarDO> findAllCars() {
		return carRepository.findAll();
	}

	@Override
	public CarDO createCar(final CarDO car) throws EntityNotFoundException {
		car.setManufacturer(manufacturerCheck(car));
		return carRepository.save(car);
	}

	@Override
	@Transactional
	public void updateCar(final CarDO car) throws EntityNotFoundException {
		CarDO updateCar = carCheck(car.getId());
		updateCar.setManufacturer(manufacturerCheck(car));
		updateCar.setConvertible(car.getConvertible());
		updateCar.setEngineType(car.getEngineType());
		updateCar.setLicensePlate(car.getLicensePlate());
		updateCar.setRating(car.getRating());
		updateCar.setSeatCount(car.getSeatCount());
	}

	@Override
	@Transactional
	public void deleteCar(final Long carId) throws EntityNotFoundException {
		CarDO car = carCheck(carId);
		car.setDeleted(Boolean.TRUE);
	}

	private CarDO carCheck(final Long carId) throws EntityNotFoundException {
		Optional<CarDO> cars = carRepository.findById(carId);
		if (!cars.isPresent()) { 
			throw new EntityNotFoundException("Could not find car with id: " + carId);
		}
		return cars.get();
	}

	private ManufacturerDO manufacturerCheck(final CarDO car) throws EntityNotFoundException {
		ManufacturerDO manufacturer = manufacturerRepository.findByName(car.getManufacturer().getName());
		if (manufacturer == null) {
			throw new EntityNotFoundException("Producer not found with this name: " + car.getManufacturer().getName());
		}
		return manufacturer;
	}

	@Override
	public CarDO findCarByLicensePlate(String licensePlate) throws EntityNotFoundException {
		Optional<CarDO> cars = carRepository.findByLicensePlate(licensePlate);
		if (!cars.isPresent()) {
			throw new EntityNotFoundException("Could not find car with License: " + licensePlate);
		}
		return cars.get();
	}

	@Override
	public List<CarDO> findByRating(Float rating) {
		return carRepository.findByrating(rating);
	}
}
