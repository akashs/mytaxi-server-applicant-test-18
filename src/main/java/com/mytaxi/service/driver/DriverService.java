package com.mytaxi.service.driver;

import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import java.util.List;

public interface DriverService {

	DriverDO findDriverById(Long driverId) throws EntityNotFoundException;

	DriverDO createDriver(DriverDO driverDO) throws ConstraintsViolationException;

	void deleteDriver(Long driverId) throws EntityNotFoundException;

	void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException;

	List<DriverDO> findDriverByOnlineStatus(OnlineStatus onlineStatus);

	List<DriverDO> findDriverByNameLicenseRating(String username, String licensePlate, Float rating);

	void updateDriverCar(long driverId, String carLicensePlate) throws CarAlreadyInUseException, EntityNotFoundException;

}
