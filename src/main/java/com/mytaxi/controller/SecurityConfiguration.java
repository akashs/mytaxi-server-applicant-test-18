package com.mytaxi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/**").hasAnyRole("DRIVER", "USER").and().csrf().disable().formLogin();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance()).withUser("driver")
				.password("driver123").roles("DRIVER").and().withUser("user1").password("user123").roles("USER");
	}

	@Autowired
	private RESTAuthenticationEntryPoint authenticationEntryPoint;

	@Component
	public class RESTAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

		@Override
		public void afterPropertiesSet() throws Exception {
			setRealmName("mytaxi-app");
			super.afterPropertiesSet();
		}
	}
}
