package com.mytaxi.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mytaxi.controller.mapper.CarMapper;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.CarService;

@RestController
@RequestMapping("v1/cars")
public class CarController {
	private static final Logger LOG = LoggerFactory.getLogger(CarController.class);
	@Autowired
	private CarService carService;

	@RequestMapping(value = "/{carId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<CarDTO> findCarById(@Valid @PathVariable long carId) throws EntityNotFoundException {
		LOG.info("getCar for carId:" + carId);
		return new ResponseEntity<>(CarMapper.populate(carService.findCarById(carId)), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<CarDTO>> findAllCars() {
		return new ResponseEntity<>(CarMapper.populate(carService.findAllCars()), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<CarDTO> createCar(@Valid @RequestBody CarDTO carDTO) throws EntityNotFoundException {
		return new ResponseEntity<>(CarMapper.populate(carService.createCar(CarMapper.convert(carDTO))),
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{carId}", method = RequestMethod.PUT)
	public ResponseEntity<Void> updateCar(@PathVariable long carId, @Valid @RequestBody CarDTO carDTO)
			throws EntityNotFoundException {
		CarDO car = CarMapper.convert(carDTO);
		car.setId(carId);
		carService.updateCar(car);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/{carId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteCar(@Valid @PathVariable long carId) throws EntityNotFoundException {
		carService.deleteCar(carId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
