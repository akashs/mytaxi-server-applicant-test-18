package com.mytaxi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mytaxi.controller.mapper.DriverMapper;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.DriverService;

/**
 * All operations with a driver will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/drivers")
public class DriverController {

	private final DriverService driverService;

	@Autowired
	public DriverController(final DriverService driverService) {
		this.driverService = driverService;
	}

	@GetMapping("/{driverId}")
	public DriverDTO getDriver(@PathVariable long driverId) throws EntityNotFoundException {
		return DriverMapper.makeDriverDTO(driverService.findDriverById(driverId));
	}

	@GetMapping("/getByParams")
	public List<DriverDTO> findDriverByNameLicenseRating(
			@RequestParam(value = "", required = false) String username,
			@RequestParam(value = "", required = false) String licensePlate,
			@RequestParam(value = "", required = false) Float rating) throws EntityNotFoundException {
		List<DriverDO> H = driverService.findDriverByNameLicenseRating(username, licensePlate, rating);
		List<DriverDTO> G = DriverMapper.makeDriverDTOList(H);
		return G;
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public DriverDTO createDriver(@Valid @RequestBody DriverDTO driverDTO) throws ConstraintsViolationException {
		DriverDO driverDO = DriverMapper.makeDriverDO(driverDTO);
		return DriverMapper.makeDriverDTO(driverService.createDriver(driverDO));
	}

	@DeleteMapping("/{driverId}")
	public void deleteDriver(@PathVariable long driverId) throws EntityNotFoundException {
		driverService.deleteDriver(driverId);
	}

	@PutMapping("updateLocation/{driverId}")
	public void updateLocation(@PathVariable long driverId, @RequestParam double longitude,
			@RequestParam double latitude) throws EntityNotFoundException {
		driverService.updateLocation(driverId, longitude, latitude);
	}

	@PutMapping("updateDriverCar/{driverId}")
	public void updateDriverCar(@PathVariable long driverId,
			@RequestParam(value = "", required = false) @Nullable String carLicensePlate)
			throws EntityNotFoundException, CarAlreadyInUseException {
		driverService.updateDriverCar(driverId, carLicensePlate);
	}

	@GetMapping
	public List<DriverDTO> findDrivers(@RequestParam OnlineStatus onlineStatus) {
		return DriverMapper.makeDriverDTOList(driverService.findDriverByOnlineStatus(onlineStatus));
	}
}
