package com.mytaxi.controller.mapper;

import java.util.ArrayList;
import java.util.List;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.CarDTO.CarDTOBuilder;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.domainvalue.EngineType;

public class CarMapper {

	public static CarDTO populate(CarDO source) {
		CarDTOBuilder carDTOBuilder = CarDTO.newBuilder().id(source.getId()).convertible(source.getConvertible())
				.engineType(EngineType.valueOf(source.getEngineType())).licensePlate(source.getLicensePlate())
				.manufacturer(source.getManufacturer().getName()).rating(source.getRating())
				.seatCount(source.getSeatCount());

		return carDTOBuilder.createDCarDTO();
	}

	public static List<CarDTO> populate(Iterable<CarDO> source) {
		List<CarDTO> CarDTOList = new ArrayList<>();
		source.forEach(car -> CarDTOList.add(populate(car)));
		return CarDTOList;
	}

	public static CarDO convert(CarDTO source) {
		CarDO car = new CarDO();
		car.setConvertible(source.getConvertible());
		car.setEngineType(source.getEngineType().getText());
		car.setLicensePlate(source.getLicensePlate());
		ManufacturerDO manufacturer = new ManufacturerDO();
		manufacturer.setName(source.getManufacturer());
		car.setManufacturer(manufacturer);
		car.setRating(source.getRating());
		car.setSeatCount(source.getSeatCount());
		return car;
	}

}
