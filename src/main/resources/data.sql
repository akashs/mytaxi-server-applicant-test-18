/**
 * CREATE Script for init of DB
 */

-- Create 3 OFFLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username) values (1000, now(), false, 'OFFLINE',
'driver01pw', 'driver01');

insert into driver (id, date_created, deleted, online_status, password, username) values (1002, now(), false, 'OFFLINE',
'driver02pw', 'driver02');

insert into driver (id, date_created, deleted, online_status, password, username) values (1003, now(), false, 'OFFLINE',
'driver03pw', 'driver03');


-- Create 3 ONLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username) values (1004, now(), false, 'ONLINE',
'driver04pw', 'driver04');

insert into driver (id, date_created, deleted, online_status, password, username) values (1005, now(), false, 'ONLINE',
'driver05pw', 'driver05');

insert into driver (id, date_created, deleted, online_status, password, username) values (1006, now(), false, 'ONLINE',
'driver06pw', 'driver06');

-- Create 1 OFFLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (1007,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'OFFLINE',
'driver07pw', 'driver07');

-- Create 1 ONLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (1008,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'ONLINE',
'driver08pw', 'driver08');

-- Create manufacturer
insert into manufacturer(id, name, date_created) values (1, 'Mercedez', now());

insert into manufacturer(id, name, date_created) values (2, 'Ferrari', now());

insert into manufacturer(id, name, date_created) values (3, 'GM', now());

-- Create Car
insert into car(id, rating, manufacturer, engine_type, license_plate, date_created, seat_count, convertible, deleted) values 
(1001, 2, 1, 'DIESEL', 'KL-11 1234', now(), 3, false, false);

insert into car(id, rating, manufacturer, engine_type, license_plate, date_created, seat_count, convertible, deleted) values 
(1002, 2, 2, 'PETROL', 'KL-11 4444', now(), 4, false, false);

insert into car(id, rating, manufacturer, engine_type, license_plate, date_created, seat_count, convertible, deleted) values 
(1003, 2, 3, 'PETROL', 'KL-11 5555', now(), 4, false, false);

insert into driver (id, date_created, deleted, online_status, password, username, car) values (1010, now(), false, 'ONLINE',
'driver10pw', 'driver10', 1001);

update car set driver=1010 where id=1001;

insert into driver (id, date_created, deleted, online_status, password, username, car) values (1011, now(), false, 'ONLINE',
'driver11pw', 'driver11', 1002);

update car set driver=1011 where id=1002;

